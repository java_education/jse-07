package ru.t1.oskinea.tm;

import ru.t1.oskinea.tm.constant.ArgumentConst;
import ru.t1.oskinea.tm.constant.CommandConst;
import ru.t1.oskinea.tm.util.TerminalUtil;

import static ru.t1.oskinea.tm.util.FormatUtil.formatBytes;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.print("Enter command: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        switch (command) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.EXIT:
                exit();
            default:
                showErrorCommand();
        }
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.printf("Current program command are not correct. See '%s'\n", CommandConst.HELP);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.out.printf("Current program arguments are not correct. See 'task-manager.jar %s'\n", ArgumentConst.HELP);
        System.exit(1);
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Evgeniy Oskin");
        System.out.println("E-mail: jizer@inbox.ru");
    }

    private static void showSystemInfo() {
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("[SYSTEM INFO]");
        System.out.println("Available processors: " + processorCount);
        System.out.println("Max memory: " + formatBytes(maxMemory));
        System.out.println("Total memory: " + formatBytes(totalMemory));
        System.out.println("Free memory: " + formatBytes(freeMemory));
        System.out.println("Used memory: " + formatBytes(usedMemory));
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show command info.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", CommandConst.EXIT);
    }

}
