package ru.t1.oskinea.tm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public interface FormatUtil {

    static BigDecimal convertBytes(final long bytes, final long base) {
        return BigDecimal.valueOf(bytes).divide(BigDecimal.valueOf(base)).setScale(2, RoundingMode.HALF_EVEN);
    }

    static String formatBytes(final long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if (bytes < kilobyte) return bytes + " B";
        else if (bytes < megabyte)
            return convertBytes(bytes, kilobyte) + " KB";
        else if (bytes < gigabyte)
            return convertBytes(bytes, megabyte) + " MB";
        else if (bytes < terabyte)
            return convertBytes(bytes, gigabyte) + " GB";
        else
            return convertBytes(bytes, terabyte) + " TB";
    }

}
